#include "processor.h"


Processor::Processor()
{
    _figureSelected = false;
    _x = 0;
    _y = 0;
}

void Processor::init()
{
    for (int i = 0; i < BOARD_SIZE; ++i)
        for (int j = 0; j < BOARD_SIZE; ++j)
            emit figureChanged(i, j, BoardHolder::instance()->getType(i, j),
                               BoardHolder::instance()->getColor(i, j));
}

void Processor::execute(int x, int y)
{
    if (_figureSelected)
    {
        if (x == _x && y == _y)
        {
            _figureSelected = false;
            emit figureSelected(x, y, false);
            return;
        }

        if (BoardHolder::instance()->moveFigure(_x, _y, x, y))
        {
            _figureSelected = false;

            emit figureSelected(_x, _y, false);

            for (int i = 0; i < BOARD_SIZE; ++i)
                for (int j = 0; j < BOARD_SIZE; ++j)
                    emit figureChanged(i, j, BoardHolder::instance()->getType(i, j),
                                       BoardHolder::instance()->getColor(i, j));
        }

    } else if (BoardHolder::instance()->canSelect(x, y))
    {
        _figureSelected = true;
        _x = x;
        _y = y;
        emit figureSelected(x, y, true);
    }

}
