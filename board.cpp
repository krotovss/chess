#include "board.h"

#include <QGridLayout>


Board::Board(QWidget *parent)
    : QWidget(parent)
{
    resize(320, 320);
    QGridLayout * mainLayout = new QGridLayout(this);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);

    _tiles.resize(BOARD_SIZE);

//    _processor = Processor;

    connect(&_processor, SIGNAL(figureChanged(int,int,figure::Type,figure::Color)),
            SLOT(setFigure(int,int,figure::Type,figure::Color)));
    connect(&_processor, SIGNAL(figureSelected(int,int,bool)),
            SLOT(selectTile(int,int,bool)));

    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        _tiles[i].resize(BOARD_SIZE);
        for (int j = 0; j < BOARD_SIZE; ++j)
        {
            _tiles[i][j] = new Tile(
                               QColor((i + j) % 2 == 0 ? "#dc9637" : "#8a100b"),
                               i, j, this);

            mainLayout->addWidget(_tiles[i][j], i, j);
            connect(_tiles[i][j], SIGNAL(clicked(int,int)),
                    &_processor, SLOT(execute(int,int)));
        }
    }

    _processor.init();
}

void Board::selectTile(int x, int y, bool isSelected)
{
    _tiles[x][y]->setSelected(isSelected);
}

void Board::setFigure(int x, int y, figure::Type type, figure::Color color)
{
    if (figure::Empty == type || figure::None == color)
    {
        _tiles[x][y]->setFigure(nullptr);
        return;
    }

    int key = makePixmapKey(type, color);
    if (!_icons.contains(key))
    {
        QString path = "://pictures/";

        switch (color)
        {
        case figure::White:
            path += "white";
            break;
        case figure::Black:
            path += "black";
            break;
        default:
            break;
        }

        path += "_";

        switch (type)
        {
        case figure::Bishop:
            path += "bishop";
            break;
        case figure::King:
            path += "king";
            break;
        case figure::Knight:
            path += "knight";
            break;
        case figure::Pawn:
            path += "pawn";
            break;
        case figure::Queen:
            path += "queen";
            break;
        case figure::Rook:
            path += "rook";
            break;
        default:
            break;
        }

        path += ".png";

        QPixmap * pixmap = new QPixmap(path);

        if (pixmap->isNull())
        {
            delete pixmap;
            return;
        }

        _icons.insert(key, pixmap);
    }

    _tiles[x][y]->setFigure(_icons[key]);
}

int Board::makePixmapKey(int a, int b)
{
    return a + (b << BOARD_SIZE);
}
