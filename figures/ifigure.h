#ifndef FIGURE_H
#define FIGURE_H

#include "boardholder.h"

#include <cstdlib>


class IFigure
{
public:
    virtual bool checkMove(int x1, int y1, int x2, int y2) const = 0;

    virtual ~IFigure() {}
};

#endif // FIGURE_H
