#ifndef KING_H
#define KING_H

#include "ifigure.h"

class King : public IFigure
{
public:
    virtual bool checkMove(int x1, int y1, int x2, int y2) const
    {
        if (x1 == x2 && y1 == y2)
            return false;

        if (std::abs(x1 - x2) <= 1 && std::abs(y1 - y2) <= 1)
        {
            if (BoardHolder::instance()->getType(x2, y2) == figure::Empty)
                return true;

            return BoardHolder::instance()->getColor(x1, y1) !=
                    BoardHolder::instance()->getColor(x2, y2);
        } if (BoardHolder::instance()->isFirstStep(x1, y1) && x1 == x2 && std::abs(y1 - y2) == 2) // Рокировка
        {
            int step = y1 < y2 ? 1 : -1;
            int pos_y = y1 + step;
            bool status = true;

            while (pos_y > 0 && pos_y < BOARD_SIZE - 1)
            {
                if (BoardHolder::instance()->getType(x1, pos_y) != figure::Empty)
                    status = false;
                pos_y += step;
            }

            if (status && BoardHolder::instance()->isFirstStep(x1, pos_y))
            {
                BoardHolder::instance()->setFigure(x1, y1 + step,
                                                   BoardHolder::instance()->getType(x1, pos_y),
                                                   BoardHolder::instance()->getColor(x1, pos_y));
                BoardHolder::instance()->removeFigure(x1, pos_y);
                return true;
            }
        }

        return false;
    }
};

#endif // KING_H
