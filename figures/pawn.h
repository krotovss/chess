#ifndef PAWN_H
#define PAWN_H

#include "ifigure.h"

class Pawn : public IFigure
{
public:
    virtual bool checkMove(int x1, int y1, int x2, int y2) const
    {
        if (x1 == x2 && y1 == y2)
            return false;

        figure::Color color = BoardHolder::instance()->getColor(x1, y1);

        if (std::abs(y1 - y2) <= 1 && std::abs(x1 - x2) <= 2)
        {
            // Проверка шага в нужную сторону
            if (color == figure::Black && x1 > x2)
                return false;
            else if (color == figure::White && x1 < x2)
                return false;

            if (y1 == y2) {
                if (std::abs(x1 - x2) == 2)
                {
                    if (BoardHolder::instance()->isFirstStep(x1, y1)
                            && BoardHolder::instance()->getType(x2, y2) == figure::Empty
                            && BoardHolder::instance()->getType((x2 + x1) / 2, y2) == figure::Empty)
                        return becomeQueen(x1, y1, x2, y2);
                    else
                        return false;
                }

                if (std::abs(x1 - x2) == 1)
                {
                    if(BoardHolder::instance()->getType(x2, y2) == figure::Empty)
                        return becomeQueen(x1, y1, x2, y2);
                    else return false;
                }
            } else if (std::abs(y1 - y2) == 1 && std::abs(x1 - x2) == 1)
            {
                if (BoardHolder::instance()->getColor(x1, y1) != BoardHolder::instance()->getColor(x2, y2)
                        && BoardHolder::instance()->getColor(x2, y2) != figure::None)
                    return becomeQueen(x1, y1, x2, y2);

                if (BoardHolder::instance()->getType(x1, y2) == figure::Pawn && BoardHolder::instance()->getColor(x1, y1) != BoardHolder::instance()->getColor(x1, y2)
                        && BoardHolder::instance()->getLastX() == x1 && BoardHolder::instance()->getLastY() == y2
                        && BoardHolder::instance()->lastMoveDistance() == 2)
                {
                    BoardHolder::instance()->removeFigure(x1, y2);
                    return becomeQueen(x1, y1, x2, y2);
                }

                return false;
            }
        }

        return false;
    }

private:
    bool becomeQueen(int x1, int y1, int x2, int) const
    {
        if (0 == x2 || 7 == x2)
        {
            BoardHolder::instance()->setFigure(x1, y1, figure::Queen, BoardHolder::instance()->getColor(x1, y1));
        }
        return true;
    }
};

#endif // PAWN_H

