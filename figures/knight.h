#ifndef KNIGHT_H
#define KNIGHT_H

#include "ifigure.h"

class Knight : public IFigure
{
public:
    virtual bool checkMove(int x1, int y1, int x2, int y2) const
    {
        if (x1 == x2 && y1 == y2)
            return false;

        if (BoardHolder::instance()->getColor(x1, y1) ==
            BoardHolder::instance()->getColor(x2, y2))
            return false;

        if (x1 != x2 && y1 != y2)
        {
            int dist = std::abs(x1 - x2) + std::abs(y1 - y2);

            if (dist == 3)
                return true;
        }

        return false;
    }

};

#endif // KNIGHT_H
