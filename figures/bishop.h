#ifndef BISHOP_H
#define BISHOP_H

#include "ifigure.h"

class Bishop : public IFigure
{
public:
    virtual bool checkMove(int x1, int y1, int x2, int y2) const
    {
        if (x1 == x2 && y1 == y2)
            return false;

        if (std::abs(x1 - x2) == std::abs(y1 - y2))
        {
            int step_x = x2 > x1 ? 1 : -1;
            int step_y = y2 > y1 ? 1 : -1;

            bool status = false;
            int pos_x = x1 + step_x;
            int pos_y = y1 + step_y;
            while (pos_x != x2)
            {
                if (BoardHolder::instance()->getColor(pos_x, pos_y) != figure::None)
                    status = true;

                pos_x += step_x;
                pos_y += step_y;
            }

            if (status == true)
                return false;

            return BoardHolder::instance()->getColor(x1, y1) !=
                    BoardHolder::instance()->getColor(x2, y2);
        }

        return false;
    }
};

#endif // BISHOP_H
