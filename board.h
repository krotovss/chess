#ifndef BOARD_H
#define BOARD_H

#include "boardholder.h"
#include "processor.h"
#include "tile.h"

#include <QMap>
#include <QVector>
#include <QWidget>


class Board : public QWidget
{
    Q_OBJECT
    
public:
    Board(QWidget *parent = 0);

private slots:
    void selectTile(int x, int y, bool isSelected);
    void setFigure(int x, int y, figure::Type, figure::Color);

private:
    QMap<int, QPixmap *> _icons;
    QVector< QVector <Tile *> > _tiles;

    Processor _processor;

    int makePixmapKey(int a, int b);
};

#endif // BOARD_H
