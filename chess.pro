#-------------------------------------------------
#
# Project created by QtCreator 2015-12-01T12:50:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = chess
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11


SOURCES += main.cpp\
        board.cpp \
    tile.cpp \
    boardholder.cpp \
    processor.cpp

HEADERS  += board.h \
    tile.h \
    figures/pawn.h \
    figures/bishop.h \
    figures/king.h \
    figures/knight.h \
    figures/queen.h \
    figures/rook.h \
    boardholder.h \
    figures/ifigure.h \
    processor.h

RESOURCES += \
    pictures.qrc
