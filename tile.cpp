#include "tile.h"

#include <QPainter>

Tile::Tile(QColor color, int x, int y, QWidget *parent) :
    QWidget(parent)
{
    _x = x;
    _y = y;
    _color = color;
    _isSelected  = false;
    _pFigure = nullptr;
}

void Tile::setFigure(QPixmap * figure)
{
    _pFigure = figure;
    update();
}

void Tile::setSelected(bool isSelected)
{
    _isSelected = isSelected;
    update();
}

void Tile::paintEvent(QPaintEvent *)
{
    QPainter painter;
    painter.begin(this);
    painter.setPen(QPen(_isSelected ? Qt::green : _color));
    painter.setBrush(QBrush(_isSelected ? Qt::green : _color));
    painter.drawRect(rect());
    if (_pFigure != nullptr)
        painter.drawPixmap(rect(), *_pFigure);
    painter.end();
}

void Tile::mousePressEvent(QMouseEvent * e)
{
    if (e->button() & Qt::LeftButton)
        emit clicked(_x, _y);
}
