#ifndef PROCESSOR_H
#define PROCESSOR_H

#include "boardholder.h"

#include <QObject>


class Processor : public QObject
{
    Q_OBJECT
public:
    Processor();

    void init();

public slots:
    void execute(int x, int y);

signals:
    void figureChanged(int x, int y, figure::Type, figure::Color);
    void figureSelected(int x, int y, bool isSelected);

private:
    bool _figureSelected;
    int _x;
    int _y;
};

#endif // PROCESSOR_H
