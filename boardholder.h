#ifndef BOARDHOLDER_H
#define BOARDHOLDER_H

#include "figures/ifigure.h"

#include <vector>

#define BOARD_SIZE 8

namespace figure {

enum Color
{
    None = 0,
    Black,
    White
};

enum Type
{
    Empty = 0,
    Bishop,
    King,
    Knight,
    Pawn,
    Queen,
    Rook
};
}

/**
 * @brief The BoardHolder class
 * Состояние шахматной доски, оценка возможности сделать конкретный ход
 */
class BoardHolder
{
public:
    static BoardHolder * instance() {
        static BoardHolder instance;

        return &instance;
    }

    figure::Color getColor(int x, int y);
    figure::Type getType(int x, int y);
    bool canSelect(int x, int y);
    bool isFirstStep(int x, int y);
    bool moveFigure(int from_x, int from_y, int to_x, int to_y);
    void removeFigure(int x, int y);
    void setFigure(int x, int y, figure::Type type, figure::Color color);

    unsigned lastMoveDistance() const;

    int getLastX() const;
    int getLastY() const;

private:

    int _lastX;
    int _lastY;
    int _fromX;
    int _fromY;
    figure::Color _playerColor;

    struct Cell
    {
        figure::Color color;
        figure::Type type;
        IFigure * figure;
        bool firstStep;

        void clear() {
            color = figure::None;
            type = figure::Empty;
            figure = nullptr;
            firstStep = false;
        }
    };

    Cell _cells[BOARD_SIZE][BOARD_SIZE];

    BoardHolder();

    void initFigures();

    bool isCorrectRequest(int x, int y);

    void createFigure(int x, int y);

};

#endif // BOARDHOLDER_H
