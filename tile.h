#ifndef TILE_H
#define TILE_H

#include <QPaintEvent>
#include <QMouseEvent>
#include <QWidget>

class Tile : public QWidget
{
    Q_OBJECT
public:
    explicit Tile(QColor color, int i, int j, QWidget *parent = 0);

    void setFigure(QPixmap * figure);
    void setSelected(bool isSelected);

signals:
    void clicked(int x, int y);
    
protected:
    virtual void paintEvent(QPaintEvent *);
    virtual void mousePressEvent(QMouseEvent *);

private:
    QColor _color;
    QPixmap * _pFigure;

    bool _isSelected;
    int _x;
    int _y;
    
};

#endif // TILE_H
