#include "boardholder.h"

#include "figures/bishop.h"
#include "figures/king.h"
#include "figures/knight.h"
#include "figures/pawn.h"
#include "figures/queen.h"
#include "figures/rook.h"


BoardHolder::BoardHolder()
{
    _lastX = 0;
    _lastY = 0;
    _fromX = 0;
    _fromY = 0;
    _playerColor = figure::White;

    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        for (int j = 0; j < BOARD_SIZE; ++j)
        {
            _cells[i][j].color = figure::None;
            _cells[i][j].type = figure::Empty;
            _cells[i][j].figure = nullptr;
            _cells[i][j].firstStep = false;
        }
    }

    initFigures();
}

void BoardHolder::initFigures()
{
    _cells[0][0].type = figure::Rook;
    _cells[0][1].type = figure::Knight;
    _cells[0][2].type = figure::Bishop;
    _cells[0][3].type = figure::Queen;
    _cells[0][4].type = figure::King;
    _cells[0][5].type = figure::Bishop;
    _cells[0][6].type = figure::Knight;
    _cells[0][7].type = figure::Rook;

    _cells[7][0].type = figure::Rook;
    _cells[7][1].type = figure::Knight;
    _cells[7][2].type = figure::Bishop;
    _cells[7][3].type = figure::Queen;
    _cells[7][4].type = figure::King;
    _cells[7][5].type = figure::Bishop;
    _cells[7][6].type = figure::Knight;
    _cells[7][7].type = figure::Rook;

    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        _cells[1][i].type = figure::Pawn;
        _cells[6][i].type = figure::Pawn;

        if (_cells[0][i].type != figure::Empty)
        {
            _cells[0][i].color = figure::Black;
            _cells[0][i].firstStep = true;
        }
        if (_cells[1][i].type != figure::Empty)
        {
            _cells[1][i].color = figure::Black;
            _cells[1][i].firstStep = true;
        }

        if (_cells[6][i].type != figure::Empty)
        {
            _cells[6][i].color = figure::White;
            _cells[6][i].firstStep = true;
        }

        if (_cells[7][i].type != figure::Empty)
        {
            _cells[7][i].color = figure::White;
            _cells[7][i].firstStep = true;
        }
    }

    for (int i = 0; i < BOARD_SIZE; ++i)
        for (int j = 0; j < BOARD_SIZE; ++j)
            createFigure(i, j);
}

bool BoardHolder::isCorrectRequest(int x, int y)
{
    if (x >= 0 && x < BOARD_SIZE && y >= 0 && y < BOARD_SIZE)
        return true;

    return false;
}

void BoardHolder::createFigure(int i, int j)
{
    switch (_cells[i][j].type)
    {
    case figure::Bishop:
        _cells[i][j].figure = new Bishop;
        break;
    case figure::King:
        _cells[i][j].figure = new King;
        break;
    case figure::Knight:
        _cells[i][j].figure = new Knight;
        break;
    case figure::Pawn:
        _cells[i][j].figure = new Pawn;
        break;
    case figure::Queen:
        _cells[i][j].figure = new Queen;
        break;
    case figure::Rook:
        _cells[i][j].figure = new Rook;
        break;
    default:
        break;
    }
}


figure::Color BoardHolder::getColor(int i, int j)
{
    if (isCorrectRequest(i, j))
        return _cells[i][j].color;

    return figure::None;
}

figure::Type BoardHolder::getType(int i, int j)
{
    if (isCorrectRequest(i, j))
        return _cells[i][j].type;

    return figure::Empty;
}

bool BoardHolder::isFirstStep(int i, int j)
{
    if (isCorrectRequest(i, j))
        return _cells[i][j].firstStep;

    return false;
}

bool BoardHolder::canSelect(int x, int y)
{
    if (isCorrectRequest(x, y))
        return getType(x, y) != figure::Empty && getColor(x, y) == _playerColor;

    return false;
}

bool BoardHolder::moveFigure(int from_x, int from_y, int to_x, int to_y)
{
    if (isCorrectRequest(from_x, from_y) && (isCorrectRequest(to_x, to_y)) &&
        canSelect(from_x, from_y))
    {
        if (!_cells[from_x][from_y].figure->checkMove(from_x, from_y, to_x, to_y))
            return false;

        _cells[to_x][to_y] = _cells[from_x][from_y];
        _cells[to_x][to_y].firstStep = false;
        removeFigure(from_x, from_y);
        _lastX = to_x;
        _lastY = to_y;
        _fromX = from_x;
        _fromY = from_y;
        if (_playerColor == figure::White)
            _playerColor = figure::Black;
        else
            _playerColor = figure::White;

        return true;
    }

    return false;
}

void BoardHolder::removeFigure(int x, int y)
{
    _cells[x][y].clear();
}

void BoardHolder::setFigure(int x, int y, figure::Type type, figure::Color color)
{
    if (!isCorrectRequest(x, y))
        return;

    _cells[x][y].clear();

    _cells[x][y].color = color;
    _cells[x][y].type = type;
    _cells[x][y].firstStep = false;
    createFigure(x, y);
}

unsigned BoardHolder::lastMoveDistance() const
{
    return abs(_fromX - _lastX) + abs(_fromY - _lastY);
}

int BoardHolder::getLastX() const
{
    return _lastX;
}

int BoardHolder::getLastY() const
{
    return _lastY;
}

