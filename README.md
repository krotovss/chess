# Chess #

Chess game for two players. Game controls only figures' movements and doesn't provide any AI.

Course work on Information System Design. It was passed in January 2016.

**Update**

* This project is a template of my code.
* Now it is at developing stage.
* Translating to English... (Some issues were in Russian and would not be translated)